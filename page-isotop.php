<?php /* Template Name: isotop*/ get_header(); ?>

<?php
	//get page slug to define loop post type and content to show
    global $post;
    $post_slug=$post->post_name;

    if ($post_slug == 'new-construction' ):

	   $the_post_type = 'newdev';

	elseif($post_slug == 'economy'):

	   $the_post_type = 'first-home';

	elseif($post_slug == 'resale'):

	   $the_post_type = 'buy';

	elseif($post_slug == 'lots'):

	   $the_post_type = 'lot';
	
	elseif($post_slug == 'north-coast'):
		$the_post_type = 'northcoast';

	endif;
?>
	<section class="buySection">
		<div class="container text-center">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col">
					<h2><?php the_title(); ?></h2>
					<div class="ownersSericeList"><?php the_field('pitch') ?></div>
					<div class="container searchForm"><?php echo do_shortcode( '[searchandfilter add_search_param="1" fields="location" hide_empty="1"  post_types="'.$the_post_type.'" submit_label="Search" all_items_labels="All Locations"]' ); ?></div>
					<div class="bd-example">
					<!-- /btn-group -->	
					  <?php if(is_user_logged_in()): ?>
					  <!-- <div class="price-container">
					  <div class="sliders row">
						<div class="col-sm-4">
							<div class="bootstrap-slider">
								<span class="filter-label">Price range: <span class="filter-selection"></span></span>
								<b class="filter-min">40</b> <input id="filter-weight" type="text" class="bootstrap-slider" value="" data-filter-group="weight"> <b class="filter-max">150</b>
					  		</div>
					  	</div>
					  
					  
					  <div id="priceGrid" class="btn-group button-group">
					    <div class="dropdown">
						  <button class="btn btn-light dropdown-toggle filtersGrid" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Price Range
						  </button>
						  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
						  	<button class="dropdown-item button is-checked" data-price="*">show all range</button>
						    <button class="dropdown-item" data-price="numberGreaterThan50" type="button">50k or less</button>
						    <button class="dropdown-item" data-price="numberGreaterThan100" type="button">100k or less</button>
						    <button class="dropdown-item" data-price="numberGreaterThan150" type="button">150k or less</button>
						    <button class="dropdown-item" data-price="numberGreaterThan200" type="button">200k or less</button>
						  </div>
						</div>
					  </div> -->

					  <?php
						$someposts = get_posts(
							array(
								'post_type' => $the_post_type,
								'posts_per_page' => -1,
								'fields' => 'ids', // return an array of ids
								'tax_query' => array(
									array(
										'taxonomy' => get_query_var('taxonomy'),
										'field' => 'slug',
										'terms' => $term_slug = get_queried_object()->slug,
									)
								)
							)
						);

						$somepoststerms = get_terms(
							array(
								'taxonomy' => 'location',
								'object_ids' => $someposts,
								'hide_empty' => 1,
							)
						);
					  ?>
					  
					  <div id="locationGrid" class="btn-group button-group">
					    <div class="dropdown">
						  <button class="btn btn-light dropdown-toggle filtersGrid" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Location
						  </button>
						  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
						  	<button class="dropdown-item button is-checked" data-location="*">Show All Locations</button>
						    <button class="dropdown-item" data-location=".cortecito" type="button">Cortecito</button>
						    <button class="dropdown-item" data-location=".cocotal" type="button">Cocotal</button>
						    <button class="dropdown-item" data-location=".costa-bavaro" type="button">Costa Bavaro</button>
						    <button class="dropdown-item" data-location=".los-corales" type="button">Los Corales</button>
							<?php 
								foreach( $somepoststerms as $localfilter ){
									echo '<button class="dropdown-item" data-location=".'.$localfilter->slug.'" type="button">'.$localfilter->name.'</button>';
								}
							?>
						  </div>
						</div>
					  </div>
					<?php endif; ?>
					</div>
					<!-- filter -->
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</section>
	<?php the_field('pricing');?>
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<section class="buySection buyListing">
		<div class="container">
		<?php
			$orig_query = $wp_query;

			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			//Query Arguments
			$args = array(
				'post_type' => $the_post_type,
				//'posts_per_page' => 18,
        		//'paged' => $paged,
        		'orderby'     => 'modified',
     			'order'       => 'DESC',

			);
			$wp_query = new WP_Query($args); 

		?>

		<?php if($wp_query->have_posts()) ?>
		   <div class="property-cards-container">
		     <?php  while ( $wp_query->have_posts() ): $wp_query->the_post(); $a++;?>
		        	<?php 
		        		$propertyLink = get_post_permalink();
		        		$propertyId =	get_the_ID();

		        		$time = get_field('time');

		        		//Get Firs tag
		        		$allposttags = get_the_tags();
		                $i=0;
		                if ($allposttags) {
		                    foreach($allposttags as $tags) {
		                        $i++;
		                        if (1 == $i) {
		                            $firsttag = $tags->name;
		                        }
		                    }
		                }
		                //Get Location Slug
		                $terms = get_the_terms( $propertyId, 'location');
						 $count = count($terms);
						 if ( $count > 0 ){
						     foreach ( $terms as $term ) {
						       $locationGrid = $term->slug;

						     }
						 }
		        	 ?>
		          	<!-- Price Card -->
					  <?php include('include/card.php');?>
					<!-- end price Card -->
		     <?php  //if($a % 3 === 0) :  echo '</div> <div class="row priceRow">'; endif; ?>
			<?php endwhile; ?>
			</div>

			<div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>
			<div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>
			<?php wp_reset_query(); ?> 
			</div>
		</div>
	</section>

	<?php endwhile; endif;  $wp_query = $orig_query; ?>

	<?php 
	get_template_part('include/optin');
	?>

	<?php get_template_part('include/zonas')?>

<?php get_footer(); ?>