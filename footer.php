			<!-- footer -->
			<footer class="footer" role="contentinfo">
				<div class="container">
					<div class="row">
						<div class="col-md">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo-footer.svg" alt="Logo" class="logo-img">
							<!-- <p>CanaBlue</p> -->
							<p class="notranslate">Avenida Barcelo Plaza San Juan Shopping Center, Local P104, Punta Cana.</p>
							<a target="_blank" class="btn btn-light blue mb-3" href="https://goo.gl/maps/FEV2uPxA4hrsWhBd9"><?php echo __('','themedomain'); ?>Get directions</a>
							<ul class="socialFooter list-inline">
								<li class="list-inline-item"><a target="_blank" href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwjUvMW39d3YAhUV9WMKHZIeBnoQFggnMAA&url=https%3A%2F%2Fwww.facebook.com%2Fcanablue%2F&usg=AOvVaw0KwCs4Y8hcQNZGUGpmFZyq"><i class="fa fa-facebook"></i></a></li>
								<li class="list-inline-item"><a target="_blank" href="https://www.instagram.com/cana_blue_punta_cana/"><i class="fa fa-instagram"></i></a></li>
								<li class="list-inline-item"><a target="_blank" href="https://www.linkedin.com/company/canablue"><i class="fa fa-linkedin"></i></a></li>
							</ul>
							<p class="copyright">&copy; <?php echo date('Y'); ?> <?php echo __('Copyright','themedomain'); ?> <?php bloginfo('name'); ?></p>
						</div>
						<div class="col-md">
							<h5><?php echo __('Company','themedomain'); ?></h5>
							<ul class="list-unstyled">
								<li><a href="/?page_id=279"><?php echo __('Meet the Team','themedomain'); ?></a></li>
								<li><a href="/?page_id=2677">Blog</a></li>
								<li><a href="/faq">FAQs</a></li>
								<li><a href="/contact"><?php echo __('Contact Us','themedomain'); ?></a></li>
							</ul>
						</div>
						<div class="col-md">
							<h5><?php echo __('Services','themedomain'); ?></h5>
							<ul class="list-unstyled">
								<li><a href="<?php echo home_url(); ?>/?page_id=10"><?php echo __('Properties for Sale','themedomain'); ?></a></li>
								<li><a href="<?php echo home_url(); ?>/?page_id=94"><?php echo __('Sell','themedomain'); ?></a></li>
								<li><a href="<?php echo home_url(); ?>/?page_id=19"><?php echo __('Rentals','themedomain'); ?></a></li>
								<li><a href="<?php echo home_url(); ?>/?page_id=106"><?php echo __('Legal','themedomain'); ?></a></li>
								<li><a href="<?php echo home_url(); ?>/?page_id=108"><?php echo __('Property Management','themedomain'); ?></a></li>
								<li><a href="<?php echo home_url(); ?>/?page_id=108"><?php echo __('Renovations','themedomain'); ?></a></li>
							</ul>
						</div>
						<div class="col-md">
							<h5><?php echo __('Neighborhoods','themedomain'); ?></h5>
							<ul class="list-unstyled">
								<li><a class="notranslate" href="<?php echo home_url(); ?>/location/palma-real-villas-cocotal">Palma Real Villas (Cocotal)</a></li>
								<li><a class="notranslate" href="<?php echo home_url(); ?>/location/costa-bavaro">Costa Bavaro</a></li>
								<li><a class="notranslate" href="<?php echo home_url(); ?>/location/el-cortecito/">El Cortecito</a></li>
								<li><a class="notranslate" href="<?php echo home_url(); ?>/location/white-sands">White Sands</a></li>
								<li><a class="notranslate" href="<?php echo home_url(); ?>/location/cap-cana">Cap Cana</a></li>
								<li><a class="notranslate" href="<?php echo home_url(); ?>/location/punta-cana">Punta Cana</a></li>

							</ul>
						</div>
					</div>
				</div>
				<!-- imessage -->
			<a class="imessage" href="sms:+1-809-975-3900"><img src="<?php echo get_template_directory_uri()?>/img/imessage.png"></a>
			</footer>
			<!-- /footer -->
			

			
			<style type="text/css">
				@supports (-webkit-overflow-scrolling: touch) {
					  #wh-widget-send-button{
					  	display: none;
					  }
				  .imessage{
						position: fixed;
						right: 5px;
						bottom:10px;
						width: 70px;
						z-index: 2000;
						display: block;
					}
					.imessage img{
						max-width: 100%;
						width: 100%;
					}
				}
				@supports not (-webkit-overflow-scrolling: touch) {
				  .imessage{
				  	display: none;
				  }
				}
			</style>
		
		<?php wp_footer(); ?>
		<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/myscript.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lightbox/js/lightbox.js"></script>
	</body>
</html>
