<?php /* Template Name: Team*/ get_header(); ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>


<section class="team-hero">
	<div>
		<h2><?php the_title();?></h2>
		<p><?php echo __('We love what we do, we are canablue','themedomain'); ?></p>
	</div>
</section>


<section class="aboutPamela">
		<div class="container">
			<?php if( have_rows('members') ): ?>
			<div class="row">
				<?php while( have_rows('members') ): the_row(); 
					// vars
					$name = get_sub_field('name');
					$nameFormated = str_replace(' ', '', $name);
					$bio = get_sub_field('bio');
					$picture = get_sub_field('picture');
					$size = 'square';
					$lang = get_sub_field('language');

					?>

					<div class="col-md-4">
						<a href="<?php echo strtolower($nameFormated);?>Moda" data-toggle="modal" data-target="#<?php echo strtolower($nameFormated);?>Modal">
							<?php echo wp_get_attachment_image( $picture, $size );?>
						</a>
						<div class="py-3 text-center">
							<h4 class="blue"><?php echo $name;?></h4>
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#<?php echo strtolower($nameFormated);?>Modal">Learn more</button>
						</div>
						<div>
						<?php
							if( $lang ): ?>
							<div class="members-lang-container">
								<?php foreach( $lang as $flag ): ?>
									<div>
										<img src="<?php echo get_template_directory_uri(); echo "/img/icon/"; echo $flag?>.png" width="38" height="38"  alt="">
									</div>
								<?php endforeach; ?>
							</div>
							<?php endif; ?>
						</div>
					</div>

					<!-- Modal -->
					<div class="modal fade" id="<?php echo strtolower($nameFormated);?>Modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo strtolower($nameFormated);?>ModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document" style="max-width:1000px">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="<?php echo strtolower($name);?>ModalLabel"><?php echo __('Work With','themedomain'); ?> <?php echo $name;?></h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<div class="row">
										<div class="col-md-6">
											<h4><?php echo __('About','themedomain'); ?> <?php echo $name;?></h4>
											<p><?php echo $bio;?></p>
										</div>
										<div class="col-md-6">
											<?php echo do_shortcode('[gravityform id="5" field_values="agent='.$name.'" title="false" description="false"]'); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>
			
		</div>
	</section>
	

<?php endwhile; endif;  ?>

	<?php get_template_part('include/optin'); ?>

	<?php get_template_part('include/zonas')?>

<?php get_footer(); ?>