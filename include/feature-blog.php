

<div class="row">
	<div class="col-12">
		<h3 class="blue pb-3"><?php echo __('Popular properties','themedomain'); ?></h3>
	</div>
</div>


<?php 
// Query Arguments
$args = array(
	'post_type' => array('buy','newdev'),
	'posts_per_page' => 3,
	'orderby' => 'rand',
	'category_name' => array('Beach Proximity', 'Golf Property', )
);

// The Query
$Beach = new WP_Query( $args );
?>
<div class="">
<?php if ( $Beach->have_posts() ): while ( $Beach->have_posts() ): $Beach->the_post();?>
	<?php 
		$propertyLink = get_post_permalink();
		$propertyId =	get_the_ID();

		$time = get_field('time');

		//Get Firs tag
		$allposttags = get_the_tags();
        $i=0;
        if ($allposttags) {
            foreach($allposttags as $tags) {
                $i++;
                if (1 == $i) {
                    $firsttag = $tags->name;
                }
            }
        }
        //Get Location Slug
        $terms = get_the_terms( $propertyId, 'location');
		 $count = count($terms);
		 if ( $count > 0 ){
		     foreach ( $terms as $term ) {
		       $locationGrid = $term->slug;

		     }
		 }
	 ?>
  	<!-- Price Card -->
	  <?php include('card.php');?>
	<!-- end price Card -->
<?php endwhile; endif;
/* Restore original Post Data */
wp_reset_postdata();
?>
</div>