<?php 
    // Query Arguments
    $location = $args['community'];
    $template_args = array(
        'post_type' => $args['property_type'],
        'posts_per_page' => 4,
        'orderby' => 'rand',
        'location' => $location
    );

    // The Query
    $Beach = new WP_Query( $template_args );
?>
<div class="property-cards-container">
<?php if ( $Beach->have_posts() ): while ( $Beach->have_posts() ): $Beach->the_post();?>
    <?php 
        $propertyLink = get_post_permalink();
        $propertyId =	get_the_ID();

        $time = get_field('time');

        //Get Firs tag
        $allposttags = get_the_tags();
        $i=0;
        if ($allposttags) {
            foreach($allposttags as $tags) {
                $i++;
                if (1 == $i) {
                    $firsttag = $tags->name;
                }
            }
        }
        //Get Location Slug
        $terms = get_the_terms( $propertyId, 'location');
        $count = count($terms);
        if ( $count > 0 ){
            foreach ( $terms as $term ) {
            $locationGrid = $term->slug;

            }
        }
    ?>
    <!-- Price Card -->
    <?php include('card.php');?>
    <!-- end price Card -->
    <?php endwhile; endif;
    /* Restore original Post Data */
    wp_reset_postdata();
    ?>
</div>