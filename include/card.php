<!-- Price Card -->

<div class="card grid-item grid-item--width2 transition <?php if (is_page(2677) or is_singular( 'post' ) ): echo "w-100"; endif; ?> <?php echo $locationGrid; ?>">
    <a href="<?php echo $propertyLink; ?>">
        <div class="card-img-top-container">
            <img class="card-img-top" src="<?php the_post_thumbnail_url('feature'); ?>" alt="Card image cap">
        </div>
        <h5><span class="badge badge-primary notranslate"><?php echo $firsttag; ?></span></h5>
        <div class="card-body">
            <h4 class="card-title notranslate"><?php the_title(); ?></h4>
            <h6 class="card-subtitle mb-2 text-muted location notranslate"><?php echo get_the_term_list( $propertyId, 'location'); ?></h6>
            <p class="card-text"><?php if($the_post_type == 'newdev'):?><?php echo __('Prices starting at','themedomain'); ?><?php endif;?> $<?php echo number_format(get_field('price'));?>
                <?php 
                if($the_post_type == 'rent'): 
                    if ( $time == 'Month' ): echo __(' /Month','themedomain'); ; elseif ( $time == 'Night' ): echo __(' /Night','themedomain');  elseif ( $time == 'Week' ): echo __(' /Week','themedomain');  endif;
                endif;
                ?>
                <?php
                    $postObj = get_post_type_object( $the_post_type ); 
                    if ( $postObj->labels->singular_name == 'Lot' ): echo __(' /per square meter','themedomain');  endif; ?>
            </p>
            <?php if($the_post_type !== "lot"): ?>
            <span class="card-link disabled"><?php echo get_field('bedrooms')?> <i class="fa fa-bed" aria-hidden="true"></i></span>
            <span class="card-link disabled"><?php echo get_field('bathrooms')?> <i class="fa fa-bath" aria-hidden="true"></i></span>
            <span style="display:none;" class="number"><?php the_field('price'); ?></span>
            <?php endif; ?>
            <?php //if($the_post_type == "newdev"): ?>
                <?php if(get_field('sale_status') == 'Only several units left'): ?><span class="badge badge-warning pull-right"><?php echo __('Only several units left','themedomain'); ?></span>
                    <?php elseif(get_field('sale_status') == 'Sold out'): ?><span class="badge badge-success pull-right"><?php echo __('Sold out','themedomain'); ?></span>
                <?php endif; ?>
            <?php //endif?>
            <?php //if($the_post_type == "buy"):?>
                    <?php if(get_field('sale_status') == 'Sale Pending'): ?><span class="badge badge-warning pull-right text-uppercase"><?php echo __('Sale Pending','themedomain'); ?></span>
                        <?php elseif(get_field('sale_status') == 'Sold'): ?><span class="badge badge-success pull-right text-uppercase"><?php echo __('Sold','themedomain'); ?></span>
                        <?php elseif(get_field('sale_status') == 'OFF THE MARKET'): ?><span class="badge badge-success pull-right text-uppercase"><?php echo __('OFF THE MARKET','themedomain'); ?></span><br>
                    <?php endif; ?>
            <?php //endif; ?>
            <!-- rented -->
            <?php if(get_field('rent_status') == 'Rented'): ?><span class="badge badge-success pull-right"><?php echo __('Rented','themedomain'); ?></span><?php endif; ?>
        </div>
    </a>
</div>
<!-- end price Card -->