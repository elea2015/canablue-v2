<section class="aboutPamela">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h4>About Pia</h4>
					<p>Pia Goris started working in Customer Service and Sales in her early 20´s. In that field she discovered her passion for excellence, customer satisfaction and her ability to follow trough on customers demands.</p>
					<p>She developed an outstanding ability to listen to the customer and think fast on the more suitable solution, which led her to the Real Estate industry.</p>
					<p>Upon arrival to Bávaro to gain experience in Hospitality her interest was redirected to the Real Estate market in Punta Cana and she was part of two renowned developments in the area with great sales success and growing relationships with colleagues.</p>
					<p>In Cana Blue she fills the role of First Home Specialist where she will guide the customers to their best option amongst all the great projects in the area destined to economic first homes.</p>
				</div>
				<div class="col-md-4">
					<img src="<?php echo get_template_directory_uri(); ?>/img/pia.png">
				</div>
			</div>
		</div>
	</section>