<?php 
get_header();

//fields
$tag_line       =   get_field('tag_line');
$video          =   get_field('video');
$gallery        =   get_field('gallery_images');
$overview       =   get_field('overview');
$overview_img   =   get_field('overview_images');
$location_title =   get_field('location_title');
$location_ben   =   get_field('location_benefits');
$investment     =   get_field('investment');
$keyFeatures    =   get_field('key_features');
$terms          =   get_field('location');
//include map
get_template_part('include/map');
?>


    <section class="community-hero my-5">
        <div class="container">
            <div class="row">
                <div class="col-md-5 community-intro">
                    <small class="blue">Community</small>
                    <h1 class="blue notranslate">Welcome to <?php the_title();?></h1>
                </div>
                <div class="col-md-7">
                   <div class="community-video-container"> 
                        <?php the_post_thumbnail('property-screenshot'); // Fullsize image for the single post ?>
                        <?php if(get_field('video')):?>
                        <a class="unitCommunityVideo" href="#" data-toggle="modal" data-target="#videoModal">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/icon/play-button.svg" alt="">
                        </a>
                        <!-- modal -->
                        <div class="modal fade" id="videoModal" tabindex="-1" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title blue"><?php the_title(); ?></h5>
                                        <button onclick="stop()" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body embed-responsive embed-responsive-16by9">
                                        <?php the_field('video'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal -->
                        <script>
                            function stop(){
                                $(".youtube-iframe")[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
                            }
                        </script>
                        <?php endif; ?>
                    </div>
                    <!-- <div class="embed-responsive embed-responsive-16by9"><?php //echo $video; ?></div> -->
                </div>
            </div>
        </div>
    </section>

    <section class="community-details">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <?php $c=0; if( have_rows('key_features') ): while( have_rows('key_features') ) : the_row(); $c++?>
                    <div class="key-features">
                        <div class="key-number">
                            <?php echo $c; ?>
                        </div>
                        <div class="key-detail">
                            <div class="key-title"><?php echo get_sub_field('feature');; ?></div>
                            <div class="key-description"><?php echo get_sub_field('description');; ?></div>
                        </div>
                    </div>
                    <?php endwhile; endif;?>
                </div>
                <div class="col-md-4">
                    <p><?php echo $tag_line; ?></p>
                </div>
            </div>
        </div>
    </section>

    <section class="community-content">
        <div class="container the-community">
            <div class="the-community-title">
                <h2 class="blue">The Community</h2>
            </div>
            <div class="the-community-menu">
                <nav class="nav">
                    <a class="nav-link blue" href="#overview">Overview</a>
                    <a class="nav-link blue" href="#location">Location</a>
                    <a class="nav-link blue" href="#properties">Properties</a>
                </nav>
            </div>
        </div>
        <div class="container community-grid">
            <?php if (have_rows('gallery_images')) :?>
               <?php while( have_rows('gallery_images') ): the_row(); ?>
               <div style="background:url(<?php echo get_sub_field('image'); ?>); background-size:cover;">
                    <!-- <img src="<?php //echo get_sub_field('image'); ?>" alt=""> -->
               </div>
                <?php endwhile;?>
            <?php endif;?>    
        </div>
        <div id="overview" class="container community-overview">
            <div class="row">
                <div class="col-md-12">
                    <small class="blue">Overview</small>
                    <h2 class="blue notranslate"><?php the_title();?></h2>
                    <p><?php echo $overview;?></p>
                </div>
            </div>
        </div>
        <div class="container community-overview_images">
            <?php if (have_rows('overview_images')) : $c = 0;?>
               <?php while( have_rows('overview_images') ): the_row(); $c++;
                $image_overview = get_sub_field('image'); ?>
               <?php if ($c == 1 || $c == 4 ):?>
                    <div class="main-img">
                        <img src="<?php echo wp_get_attachment_image_url($image_overview, 'slider-size'); ?>" alt="">
                    </div>
               <?php else:?>
               <div>
                    <img src="<?php echo wp_get_attachment_image_url($image_overview, 'square'); ?>" alt="">
               </div>
               <?php endif; endwhile;?>
            <?php endif;?>    
        </div>
        <div id="location" class="container community-location">
            <small class="blue">Location</small>
            <h3 class="blue"><?php echo $location_title;?></h3>
            <p><?php echo $location_ben;?></p>
            <div class="community-overview_images">
                <?php if (have_rows('location_images')) : $c = 0;?>
                <?php while( have_rows('location_images') ): the_row(); $c++;
                    $image_overview = get_sub_field('image'); ?>
                <?php if ($c == 1 || $c == 4 ):?>
                        <div class="main-img">
                            <img src="<?php echo wp_get_attachment_image_url($image_overview, 'slider-size'); ?>" alt="">
                        </div>
                <?php else:?>
                    <div>
                            <img src="<?php echo wp_get_attachment_image_url($image_overview, 'square'); ?>" alt="">
                    </div>
                <?php 
                    endif; 
                endwhile;
                endif;
                ?>  
            </div>
            <div class="community-map">
            <?php 
                $location = get_field('map');

                if( !empty($location) ):
                ?>
                <div class="acf-map">
                    <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                </div>
            <?php endif; ?>	
            </div>
        </div>
        <div id="properties" class="container community-properties">
            <small class="blue">Properties</small>
            <h3 class="blue">Investment oportunities in this location</h3>
            <p><?php echo $investment;?></p>
            <div class="community-properties mb-4">
                <?php 
                get_template_part(
                    'include/community-properties', 
                    'name', 
                    array( 
                        'property_type' => 'newdev',
                        'community' => $terms->slug,

                    )
                )
                ?>
                <div class="text-center">
                    <a href="/location/<?php echo $terms->slug; ?>/?s=&post_types=newdev" class="btn btn-primary">View More Property Developments</a>
                </div>
            </div>

            <div class="community-properties mb-4">
                <?php 
                get_template_part(
                    'include/community-properties', 
                    'name', 
                    array( 
                        'property_type' => 'buy',
                        'community' => $terms->slug,

                    )
                )
                ?>
                <div class="text-center">
                    <a href="/location/<?php echo $terms->slug; ?>/?s=&post_types=buy" class="btn btn-primary">View More Resale Properties</a>
                </div>
            </div>
            
            
        </div>
    </section>


<?php get_footer();?>