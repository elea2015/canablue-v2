
<?php /* Template Name: Feedback */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="container my-5">

    		<div class="row justify-content-center">	
    		    <div class="col-md-6">
    		        <h1 class="text-center blue"><?php the_title(); ?></h1>
    			
    			    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
    			
        			<!-- article -->
        			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        			
        				<?php the_content(); ?>
        			
        				<?php comments_template( '', true ); // Remove if you don't want comments ?>
        			
        				<br class="clear">
        			
        				<?php edit_post_link(); ?>
        			
        			</article>
        			<!-- /article -->
        			
        					<?php endwhile; ?>
        			
        					<?php else: ?>
        			
        			<!-- article -->
        			<article>
        			
        				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
        			
        			</article>
        			<!-- /article -->
    			
    			<?php endif; ?>
    		    </div>
			</div>

		</section>
		<!-- /section -->
	</main>
	<style>
	    .swp_social_panel{display:none;}
	</style>



<?php get_footer(); ?>
