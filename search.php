<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>
			<?php 

			$locationHeading = preg_replace('/[^\p{L}\p{N}\s]/u', ' ', $_GET["location"]);
			$postType = $_GET["post_types"];


			?>
			<h1 class="text-center locationHeading py-5 blue"><?php echo 'Results'//$locationHeading; ?></h1>

			<?php if (have_posts()):  ?>

			<section class="buySection buyListing">
					<div class="container">
				
					   <div class="property-cards-container">
					     <?php  while (have_posts()) : the_post(); $a++;?>
					        
					        	<?php 
					        		$propertyLink = get_post_permalink();
					        		$propertyId =	get_the_ID();

					        		$time = get_field('time');

					        		$allposttags = get_the_tags();
					                $i=0;
					                if ($allposttags) {
					                    foreach($allposttags as $tags) {
					                        $i++;
					                        if (1 == $i) {
					                            $firsttag = $tags->name;
					                        }
					                    }
					                }
					        	 ?>
					          	<!-- Price Card -->
								  <?php include('include/card.php');?>
								<!-- end price Card -->
					        
					     <?php  //if($a % 3 === 0) :  echo '</div> <div class="row priceRow">'; endif;
					      ?>
						<?php endwhile; endif ?>
						</div>
					</div>
				</div>
				</section>

<?php get_footer(); ?>
