<?php /* Template Name: home */ get_header(); ?>

	<?php get_template_part('include/slider'); ?>
	<div class="container searchForm wow fadeInUp"><?php echo do_shortcode( '[searchandfilter add_search_param="1" fields="post_types,location,type"  post_types="buy,newdev,first-home,rent" submit_label="Search" all_items_labels="All Properties, All Locations, All Types"]' ); ?></div>
	<?php get_template_part('include/optin'); ?>

	<section class="discoverHow homeSection wow fadeInUp">
		<div class="container text-center">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col">
					<h2><?php the_field('discover_heading');?></h2>
					<br>
					<p><?php the_field('discover_text');?></p>
					<br>
					<a href="<?php echo site_url();?>/?page_id=10" class="btn btn-primary">Discover Now</a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</section>

	<section class="container my-5"><?php get_template_part('include/feature') ?></section>

	<section id="ownersForm" class="container localExpert wow fadeInUp">
		<div class="row">
			<div class="col-2"></div>
			<div class="col-md-8">
				<h3 class="text-center blue">Contact us today</h3>
				<p class="text-center"></p>
				<div class="ownersForm wow fadeInUp">
					<p><i class="fa fa-mobile"></i> 809-975-3900 </p>
					<a href="mailto:info@canablue.com"><i class="fa fa-envelope-o"></i> info@canablue.com</a>
					<?php
					$pageName = get_the_title();
					echo do_shortcode('[gravityform id="6" field_values="property='.$pageName.'" title="false" description="false" ajax="true"]');
					?>
				</div>
			</div>
		</div>
	</section>





	<?php get_template_part('include/zonas')?>
	
	<!-- Services -->
	<?php get_template_part('include/services'); ?>
	
	<!-- About Pamela -->
	<?php get_template_part('include/pamela'); ?>

<?php get_footer(); ?>
