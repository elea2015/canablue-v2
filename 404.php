<?php get_header(); ?>

	<main role="main container py-5 my-5">
		<!-- section -->
		<section class="row py-5 my-5 justify-content-center">

			<!-- article -->
			<article class="col-6 text-center py-5 my-5" id="post-404">

				<h1><?php _e( 'Page not found', 'html5blank' ); ?></h1>
				<a class="btn btn-primary" href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a>
				<div class="searchForm wow"><?php echo do_shortcode( '[searchandfilter add_search_param="1" fields="post_types,location,type"  post_types="newdev,first-home" submit_label="Search" all_items_labels="All Properties, All Locations, All Types"]' ); ?></div>
				

			</article>
			<!-- /article -->

		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
